/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.specialassignment2;

import java.io.IOException;
import java.io.Serializable;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *  Backing bean for the index.xhtml view
 * 
 * @author 1632647
 */
@Named("formModel")
@SessionScoped
public class FormModel implements Serializable {
    private String number;
    private String validationString;
    private String numberRegEx;
    private Pattern p;
    
    @PostConstruct
    public void init() {
        numberRegEx = "\\b([0-9]{1,2}|[1-6][0-9]{2}|7[0-3][0-9]|74[0-4])\\b";
        p = Pattern.compile(numberRegEx);
    }
    /* Getter and Setter*/
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    
    public String getValidationString() {
        return validationString;
    }

    public void setValidationString(String validationString) {
        this.validationString = validationString;
    }
    
    /**
     * Validate if the string match the regex pattern
     * 
     * @throws IOException 
     */
    public void validate() throws IOException {
        if(number.matches(p.pattern())) {
            validationString = "The number is valid!";
        } else {
            validationString = "Invalid number!";
        }
        number = "";
        FacesContext.getCurrentInstance().getExternalContext().redirect("output.xhtml");
    }
}
